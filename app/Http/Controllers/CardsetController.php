<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCardsetRequest;
use App\Http\Requests\UpdateCardsetRequest;
use App\Models\Cardset;

class CardsetController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCardsetRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Cardset $cardset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Cardset $cardset)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCardsetRequest $request, Cardset $cardset)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Cardset $cardset)
    {
        //
    }
}
